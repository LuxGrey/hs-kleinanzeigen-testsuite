# HS Kleinanzeigen testsuite
Die Anwendung erlaubt es, die HS-Kleinanzeigen Anwendung als Blackbox zu testen. 
Die Tests sind nach Praktikumsaufgaben gegliedert.

## Voraussetzungen/Setup
* JDK 11
* Maven
* Als Base-URI der zu testenden Applikation wird "http://localhost:8081/" angenommen.
* Als Datenbank der zu testenden Applikation wird eine lokale MySQL Instanz die auf Port 3306 erreichbar ist angenommen.

## Setup der lokalen Umgebung testen
Das korrekte Setup der lokalen Entwicklungsumgebung kann mit Hilfe des folgenden Befehls getestet werden:

`sh check_requirements.sh`