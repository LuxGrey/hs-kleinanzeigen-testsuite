package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.fail;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum1Test {

    @Autowired
    private DataSource dataSource;

    private Connection connection;

    /**
     * Establishes a direct SQL connection to query Database metadata and sets the RestAssured authentication.
     *
     * @throws SQLException if the DBMS connection failed
     */
    @BeforeEach
    void setUp() throws SQLException {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;
        RestAssured.authentication = RestAssured.DEFAULT_AUTH;
    }

    /**
     * closes the sql connection
     *
     * @throws SQLException if the DBMS connection didn't exist
     */
    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * This tests checks if the provided connection is valid by running a query that is always successful.
     */
    @Test
    void task6_checkDatabaseConnectionSuccessful() {
        try (Statement statement = connection.createStatement()) {
            statement.executeQuery("SELECT 1");
        } catch (SQLException e) {
            fail("SQL-Connection unsuccessful");
        }
    }

    /**
     * This test checks if the spring boot application is running by querying the info actuator.
     */
    @Test
    void task7_checkApplicationRunning() {
        given().when().get("/hs-kleinanzeigen/actuator/info")//
                .then().statusCode(HttpStatus.OK.value());
    }
}
