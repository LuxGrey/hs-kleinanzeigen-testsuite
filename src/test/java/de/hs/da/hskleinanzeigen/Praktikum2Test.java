package de.hs.da.hskleinanzeigen;

import io.restassured.RestAssured;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@SpringJUnitConfig
@SpringBootTest
@Transactional
class Praktikum2Test {

    private static final String payload = "{\n" +
            "   \"type\":\"Offer\",\n" +
            "   \"category\": 1,\n" +
            "   \"title\":\"Zimmer in 4er WG\",\n" +
            "   \"description\":\"Wohnheim direkt neben der HS\",\n" +
            "   \"price\":400,\n" +
            "   \"location\":\"Birkenweg, Darmstadt\"\n" +
            "}\n";
    private static final String payload_withUser = "{\n" +
            "    \"type\" : \"Offer\",\n" +
            "    \"category\" : 1,\n" +
            "    \"title\" : \"test post ad\",\n" +
            "    \"description\" : \"test post ad\",\n" +
            "    \"price\" : 42,\n" +
            "    \"location\" : \"FFF\",\n" +
            "    \"user\" : 1\n" +
            "}";
    private static final String payload_incomplete = "{\n" +
            "   \"category\":1,\n" +
            "   \"title\":\"Zimmer in 4er WG\",\n" +
            "   \"description\":\"Wohnheim direkt neben der HS\",\n" +
            "   \"price\":400,\n" +
            "   \"location\":\"Birkenweg, Darmstadt\"\n" +
            "}\n";
    private final ArrayList<String> expectedTables = new ArrayList<>() {
        {
            add("AD");
            add("CATEGORY");
        }
    };


    @Autowired
    private DataSource dataSource;

    private Connection connection;

    private boolean initialized = false;

    @BeforeEach
    void setUp() throws IOException, SQLException {
        connection = dataSource.getConnection();
        Assertions.assertNotNull(connection);

        RestAssured.baseURI = TestUtil.HOST;
        RestAssured.port = TestUtil.PORT;

        if (TestUtil.isSecurityEnabled()) {
            TestUtil.setAuthenticationForUser();
        } else {
            TestUtil.setNoAuthentication();
        }

        if (TestUtil.doesColumnUserExist(connection)) {
            expectedTables.add("USER");
        }

        prepareData();
    }

    private void prepareData() throws SQLException {
        if (!initialized) {
            TestUtil.insertData(connection, expectedTables);
            initialized = true;
        }
    }

    @AfterEach
    void tearDown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    /**
     * checks if the required tables (defined in expectedTables) exist in the Database
     *
     * @throws SQLException if the connection failed
     */
    @Test
    void task1_checkRequiredTablesExist() throws SQLException {
        ArrayList<String> existingTables = TestUtil.checkExistingTables(connection, expectedTables);

        Assertions.assertEquals(expectedTables.size(), existingTables.size());
    }

    /**
     * expected Table layout:
     * ID	Integer, Primary Key, auto increment
     * TYPE	Enumeration (Offer, Request), not null
     * CATEGORY_ID	Integer, Foreign Key auf Tabelle CATEGORY, not null
     * TITLE	Varchar, not null
     * DESCRIPTION	Varchar, not null
     * PRICE	Decimal
     * LOCATION	Varchar
     * CREATED	Timestamp, not null
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_checkAdColumnsExist() throws SQLException {
        // TODO asserts all together
        // TODO print out exact error (not 'or')
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "ID", TestUtil.FieldType.INTEGER), "Column ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "TYPE", TestUtil.FieldType.ENUMERATION), "Column TYPE is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "CATEGORY_ID", TestUtil.FieldType.INTEGER), "Column CATEGORY_ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "TITLE", TestUtil.FieldType.VARCHAR), "Column TITLE is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "DESCRIPTION", TestUtil.FieldType.VARCHAR), "Column DESCRIPTION is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "PRICE", TestUtil.FieldType.DECIMAL), "Column PRICE is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "LOCATION", TestUtil.FieldType.VARCHAR), "Column LOCATION is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "AD", "CREATED", TestUtil.FieldType.TIMESTAMP), "Column CREATED is missing or has invalid data type");
    }

    /**
     * expected Table layout:
     * ID	Integer, Primary Key, auto increment
     * PARENT_ID	Integer, Foreign Key auf Tabelle CATEGORY, nullable
     * NAME	Varchar
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task1_checkCategoryColumnsExist() throws SQLException {
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "CATEGORY", "ID", TestUtil.FieldType.INTEGER), "Column ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "CATEGORY", "PARENT_ID", TestUtil.FieldType.INTEGER), "Column PARENT_ID is missing or has invalid data type");
        Assertions.assertTrue(TestUtil.checkFieldExist(connection, "CATEGORY", "NAME", TestUtil.FieldType.VARCHAR), "Column NAME is missing or has invalid data type");
    }

    /**
     * checks the rest endpoint for creating an advertisement
     *
     * @throws SQLException if the connection failed or the table doesn't exist
     */
    @Test
    void task3_createAdResult201() throws SQLException {
        if (TestUtil.doesColumnUserExist(connection)) {

            given()
                    .basePath(TestUtil.BASE_PATH_AD)
                    .header("Content-Type", "application/json")
                    .body(payload_withUser)
                    .accept("application/json")
                    .when().post()
                    .then().statusCode(HttpStatus.CREATED.value())
                    .contentType("application/json")
                    .body("id", notNullValue())
                    .body("type", allOf(oneOf("Offer", "Request"), notNullValue()))
                    .body("category", notNullValue())
                    .body("title", equalTo("test post ad"))
                    .body("description", equalTo("test post ad"))
                    .body("price", equalTo(42))
                    .body("location", equalTo("FFF"))
                    .body("user", notNullValue());

        } else {
            given()
                    .basePath(TestUtil.BASE_PATH_AD)
                    .header("Content-Type", "application/json")
                    .body(payload)
                    .accept("application/json")
                    .when().post()
                    .then().statusCode(HttpStatus.CREATED.value())
                    .body("id", notNullValue())
                    .body("type", equalTo("Offer"))
                    .body("category", notNullValue())
                    .body("title", equalTo("Zimmer in 4er WG"))
                    .body("description", equalTo("Wohnheim direkt neben der HS"))
                    .body("price", equalTo(400))
                    .body("location", equalTo("Birkenweg, Darmstadt"));
        }
    }

    /**
     * checks if creating an incomplete advertisement returns HTTP.400 (Bad Request)
     */
    @Test
    void task3_createAdResult400() {
        given()
                .basePath(TestUtil.BASE_PATH_AD)
                .header("Content-Type", "application/json")
                .body(payload_incomplete)
                .when().post()
                .then().statusCode(HttpStatus.BAD_REQUEST.value());

    }

    /**
     * checks if the rest-endpoint for getting an existing advertisement works
     */
    @Test
    void task4_getAdResult200() {

        given()
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", 1)
                .accept("application/json")
                .when().get("{id}")
                .then().statusCode(HttpStatus.OK.value())
                .body("id", notNullValue())
                .body("type", equalTo("Offer"))
                .body("category", notNullValue())
                .body("title", equalTo("Titel"))
                .body("description", equalTo("Beschreibung"))
                .body("price", equalTo(42))
                .body("location", equalTo("Standort"));
    }

    /**
     * checks if the rest-endpoint for querying a not existing advertisement returns Http.404 (Not found)
     */
    @Test
    void task4_getAdResult404() {
        given()
                .accept("application/json")
                .basePath(TestUtil.BASE_PATH_AD)
                .pathParam("id", -1)
                .when().get("{id}")
                .then().statusCode(HttpStatus.NOT_FOUND.value());
    }
}
